package app.sampleproject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * Created by Endra on 2019-10-29.
 */
public class MainFragment extends AppCompatActivity {

    BottomNavigationView navview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainfragmen);

        navview = findViewById(R.id.bottomNavigationView);

        ///setup firstUI
        setupFirstUI();

        //setup navigation view
        navview.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Fragment fragment = null;
                switch (menuItem.getItemId()){
                    case R.id.home:
                        fragment = new HomeFragment();
                        break;
                    case R.id.transaksi:
                        fragment = new TrxFragment();
                        break;
                    case R.id.profile:
                        fragment = new ProfileFragment();
                        break;
                }
                @SuppressLint("ResourceType")
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().setCustomAnimations(R.transition.enter_right_to_left,R.transition.exit_right_to_left,R.transition.enter_left_to_right,R.transition.exit_left_to_right);
                transaction.addToBackStack(null);
                transaction.replace(R.id.frame,fragment).commit();
                return true;
            }
        });
    }

    private void setupFirstUI() {
        if (getSupportFragmentManager() != null){
            getSupportFragmentManager().beginTransaction().replace(R.id.frame, new HomeFragment()).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_menu, menu);
        return true;
    }
}
