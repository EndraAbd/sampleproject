package app.sampleproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private EditText editEmail, editPassword;
    private Button btLogin;

    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Kenali Komponen
        initUI();

        //inisialiasi Request
        initRequest();

        //set OnClick TOmbol
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jsonParser();
            }
        });
    }

    private void jsonParser() {
        final String email = editEmail.getText().toString().trim();
        final String password = editPassword.getText().toString().trim();

        if (email.isEmpty() || password.isEmpty()){
            //muculkan Pesan email password tidak boleh kosong
            Toast.makeText(this,"Email atau password wajib diisi",Toast.LENGTH_LONG).show();
        }else{
            String url = "http://notifwa.com/api/login.php";

            //bungkus parameter ke array
            HashMap<String,String> params = new HashMap<>();
            params.put("email",email);
            params.put("password",password);

            Log.d("parameter",String.valueOf(params));

            //konversi array ke object
            final JSONObject jsonObject = new JSONObject(params);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    parserData(response,email,password);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //show message for error
                    Toast.makeText(MainActivity.this,"TOlong beli pulsa dulu",Toast.LENGTH_LONG).show();
                }
            });

            requestQueue.add(jsonObjectRequest);
        }
    }

    private void parserData(JSONObject response, String email, String password) {
        try{
            String status = response.getString("code");
            String res    = response.getString("response");
            String msg    = response.getString("message");
//            Toast.makeText(this,status, Toast.LENGTH_LONG).show();
            if (status.equals("1")){
                Toast.makeText(this,msg, Toast.LENGTH_LONG).show();
                callNewActivity();
            }else{
                Toast.makeText(this,msg, Toast.LENGTH_LONG).show();
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void callNewActivity() {
        Intent listuser = new Intent(this,ListUser.class);
        startActivity(listuser);
    }

    private void initRequest() {
        requestQueue = Volley.newRequestQueue(this);
    }

    private void initUI() {
        editEmail = findViewById(R.id.edEmail);
        editPassword = findViewById(R.id.edPassword);
        btLogin = findViewById(R.id.btnLogin);
    }
}
